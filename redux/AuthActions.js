import { LOGIN } from './types';
// import { SEARCH_IMAGES } from './types';

export const login = input => (
  {
    type: LOGIN,
    payload: input,
  }
);

// export const searchImages = input => (
//   {
//     type: SEARCH_IMAGES,
//     payload: input,
//   }
// );

// export const deleteFriend = friendsIndex => (
//   {
//     type: DELETE_FRIEND,
//     payload: friendsIndex,
//   }
// )