import { combineReducers } from 'redux';
import { LOGIN } from './types';
// import { SEARCH_IMAGES } from './types';
// import Unsplash, { toJson } from 'unsplash-js/native';

const INITIAL_STATE = {
  userToken: null,
  isLoading: true
}

const AuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      state.userToken = '123'
      state.isLoading = false
      return state
    default:
      return state
  }
};

export default combineReducers({
  auth: AuthReducer,
});


// const INITIAL_STATE = {
//   images: ''
// }

// const imagesReducer = (state = INITIAL_STATE, action) => {
//   switch (action.type) {
//     case SEARCH_IMAGES:
//       const unsplash = new Unsplash({ accessKey: `oJp_SKgXbQDVWOJyoR6R05lc1c6mw1nsRfCann9CSGQ` });
//       unsplash.search.photos(action.payload, 1, 21, { orientation: "portrait" })
//         .then(toJson)
//         .then(json => INITIAL_STATE.images = json.results)
//     default:
//       return state
//   }
// };

// export default combineReducers({
//   images: imagesReducer,
// });
