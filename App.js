import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AuthReducer from './redux/AuthReducer';
import HomeScreen from './components/HomeScreen';
import DetailImageScreen from './components/DetailImageScreen';
import LoginScreen from './components/LoginScreen';
import SplashScreen from './components/SplashScreen';
import AboutScreen from './components/AboutScreen';
import Icon from 'react-native-vector-icons/FontAwesome5'

const store = createStore(AuthReducer);

const AuthStack = createStackNavigator();
const AuthStackScreen = ({ userToken }) => (
  <AuthStack.Navigator initialRouteName='Home' headerMode='none'>
    
    { userToken ? (
      <AuthStack.Screen name='Home' component={HomeScreen} />
    ) : (
      <AuthStack.Screen 
        name='Login' 
        component={HomeStackScreen}
        // options={{ title: 'Sign In' }}
      />
      )
    }

  </AuthStack.Navigator>
) 

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator initialRouteName="Home" tabBarOptions={{activeTintColor: '#F77866', style: {paddingBottom: 9, paddingTop: 9}}}>
    <Tabs.Screen name='Home' component={ImageStackScreen} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="home" color={focused ? '#F77866' : '#727C8E'} size={21} />
      )}} 
    />
    <Tabs.Screen name='Profile' component={AboutScreen} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="user-astronaut" color={focused ? '#F77866' : '#727C8E'} size={21} />
      )}} />
  </Tabs.Navigator>
)

const ImageStack = createStackNavigator();
const ImageStackScreen = () => (
  <ImageStack.Navigator initialRouteName='Home' headerMode='none'>
      <ImageStack.Screen name='Home' component={HomeScreen} />
      <ImageStack.Screen name='Detail' component={DetailImageScreen} />
  </ImageStack.Navigator>
) 


const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator initialRouteName='Login' headerMode='none'>
    <HomeStack.Screen 
        name='Login' 
        component={LoginScreen}
        // options={{ title: 'Sign In' }}
      />
    <HomeStack.Screen
      name="iPictures"
      component={TabsScreen}
    />
  </HomeStack.Navigator>
)

export default App = () => {
  const [userToken, setUserToken] = React.useState(store.getState().auth.login)
  const [isLoading, setIsLoading] = React.useState(store.getState().auth.isLoading)

  // Start loading
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])
  if(isLoading) {
    return <SplashScreen />
  }
  // End loading

  return (
    <Provider store={store}>
      <NavigationContainer>
        <AuthStackScreen userToken={userToken} />
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
