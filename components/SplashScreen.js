import React from 'react'
import { View, Image, Dimensions } from 'react-native'

export default SplashScreen = () => (
    <View style={{flex: 1}}>
        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
            <View style = {{
                borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
                width: Dimensions.get('window').width * 0.9,
                height: Dimensions.get('window').width * 0.9,
                backgroundColor:'rgba(33,31,101,0.1)',
                justifyContent: 'center',
                alignItems: 'center'
                }}
            >
                <Image source={require('../android/app/src/main/assets/images/iconmonstr-sound-wave-6-240.png')} />
            </View>
        </View>
        <View style={{flexDirection: 'row', height: 5, width: 134, backgroundColor: 'black', marginVertical: 24, alignSelf: 'center', borderRadius: 7}} />
    </View>
)