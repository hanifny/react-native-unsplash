import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, Image, TouchableOpacity, PermissionsAndroid, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';

const DetailImageScreen = (props) => {
    console.log(props);
    const [state, setState] = useState( {
        url: props.route.params.urls.regular,
        saving: false,
      }
    )
    
      let updateUrl = url => {
        setState({url});
      };
    
      let getPermissionAndroid = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: 'Image Download Permission',
              message: 'Your permission is required to save images to your device',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
          }
          Alert.alert(
            'Save remote Image',
            'Grant Me Permission to save Image',
            [{text: 'OK', onPress: () => console.log('OK Pressed')}],
            {cancelable: false},
          );
        } catch (err) {
          Alert.alert(
            'Save remote Image',
            'Failed to save Image: ' + err.message,
            [{text: 'OK', onPress: () => console.log('OK Pressed')}],
            {cancelable: false},
          );
        }
      };
    
      let handleDownload = async () => {
        // if device is android you have to ensure you have permission
        if (Platform.OS === 'android') {
          const granted = await getPermissionAndroid();
          if (!granted) {
            return;
          }
        }
        setState({saving: true});
        RNFetchBlob.config({
          fileCache: true,
          appendExt: 'png',
        })
          .fetch('GET', state.url)
          .then(res => {
            CameraRoll.save(res.data, 'photo')
              .then(() => {
                Alert.alert(
                  'Save remote Image',
                  'Image Saved Successfully',
                  [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                  {cancelable: false},
                );
              })
              .catch(err => {
                Alert.alert(
                  'Save remote Image',
                  'Failed to save Image: ' + err.message,
                  [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                  {cancelable: false},
                );
              })
              .finally(() => setState({saving: false}));
          })
          .catch(error => {
            setState({saving: false});
            Alert.alert(
              'Save remote Image',
              'Failed to save Image: ' + error.message,
              [{text: 'OK', onPress: () => console.log('OK Pressed')}],
              {cancelable: false},
            );
          });
      };
    
    return (
        <View style={styles.container}>
        
        <View style={styles.body}>
            <Image
                style={styles.avatar} 
                source={{ uri: props.route.params.user.profile_image.small }}
            />
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 21}}>  { props.route.params.user.name }</Text>
                {props.route.params.user.location ? <Text>   ${props.route.params.user.location} </Text> : null} 
            </View>
        </View>

        <Image 
            style={styles.image} 
            source={{ uri: props.route.params.urls.regular }} 
        />

        <View style={styles.content}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon name="heart" size={30} color="red" solid />
                <Text> {props.route.params.likes} likes </Text>
            </View>
            <Text> {moment(props.route.params.created_at).fromNow(true)} ago </Text>
        </View>

        <View style={styles.content}>
            <View>
                <Text style={{fontWeight: 'bold'}}> {props.route.params.user.name} </Text>
                <Text> {props.route.params.description} </Text>
            </View>
        </View>
        
        <TouchableOpacity style={{marginTop: 21, width: 277}}>
            <Button title="Download" onPress={() => handleDownload()} color='#28df99' />
        </TouchableOpacity>

        </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    flexDirection: 'row', 
    width: 251, 
    alignItems: 'center'
  },
  content: {
      flexDirection: 'row', 
      width: 251, 
      alignItems: 'center', 
      justifyContent: 'space-between',
      marginVertical: 5
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    overflow:'hidden',
  },
  image: {
    width: 251,
    height: 311,
    marginVertical: 7,
    marginTop: 13  
  }
});

export default DetailImageScreen