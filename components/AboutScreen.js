import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default about = () => {
    return (
        <View style={styles.container}>
            {/* <View style={styles.body}> */}
                <View style={styles.profile}>
                    <Icon name="account-circle" color="#00ACEE" size={121} />
                    <Text>Hanif Nuryanto</Text>
                    <Text style={{fontWeight: 'bold'}}>React Native Developer</Text>
                </View>
                <View style={styles.social}>
                    <Text>Social Accounts</Text>
                    <View style={{height:0.5, width: 100,backgroundColor: "#00ACEE"}}/>
                    <View style={styles.socialRow}>
                        <Image source={require('../android/app/src/main/assets/images/fb.png')} style={styles.socialIcon} />
                        <Text>  facebook.com/hanifnuryanto </Text>
                    </View>
                    <View style={{height:0.5, marginTop: 7.5, width: 145,backgroundColor: "#BDBDBD"}}/>
                    <View style={styles.socialRow}>
                    <Image source={require('../android/app/src/main/assets/images/twitter.png')} style={styles.socialIcon} />
                    <Text>  twitter.com/hanifnuryanto </Text>
                    </View>
                    <View style={{height:0.5, marginTop: 7.5, width: 145,backgroundColor: "#BDBDBD"}}/>
                    <View style={styles.socialRow}>
                        <Image source={require('../android/app/src/main/assets/images/github.webp')} style={styles.socialIcon} />
                        <Text>  github.com/hanifny </Text>
                    </View>
                </View>
            {/* </View> */}
            <View style={styles.tabBar}>
                <Text style={{fontWeight: 'bold'}}>iPictures</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: "center",
      alignItems: "center"
    },
    profile: {
      alignItems: 'center',
    },
    social: {
      marginVertical: 39,
      marginHorizontal: 15,
      padding: 17,
      backgroundColor: '#EFEFEF',
      borderRadius: 25
    },
    socialRow: {
      flexDirection: 'row',
      marginTop: 15,
      alignItems: 'center'
    },  
    socialIcon: {
      width: 31,
      height: 31
    },
    tabBar: {
      flexDirection: 'row',
      justifyContent: 'center'
    }, 
  });
  