import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, Image, FlatList } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Unsplash, { toJson } from 'unsplash-js/native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const HomeScreen = (props) => {
  const unsplash = new Unsplash({ accessKey: `oJp_SKgXbQDVWOJyoR6R05lc1c6mw1nsRfCann9CSGQ` });
  
  const [input, setInput] = useState('')
  const [images, setImages] = useState(null)

  const searchImages = (payload) => (
    unsplash.search.photos(payload, 1, 21, { orientation: "portrait" })
    .then(toJson)
    .then(json => setImages(json.results))
  )

  const ImagesList = ( img ) => (
    <TouchableOpacity onPress={() => props.navigation.navigate('Detail', img.img)} >
      <Image 
        key={ img } 
        style={{
          width: 251,
          height: 311,
          resizeMode: 'stretch',
          marginVertical: 7
        }} 
        source={{ uri: img.img.urls.thumb }} />
    </TouchableOpacity>
  )

  const renderItem = ( img ) => (
    <ImagesList img={img.item} />
  )

  return (
    <View style={styles.container}>
      
      <View style={{flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: 1, marginTop: 21}}>
        <TextInput
          style={{height: 40,}}
          placeholder="Type here to search images! "
          onChangeText={text => {
            setInput(text)
            searchImages(text)
          }}
          defaultValue={input}
        />
        <Icon name='search' size={21} color='black' />
        
      </View>
          
      {input ? (
        <FlatList
          data={images}
          renderItem={renderItem} 
          keyExtractor={img => img.id}
        />
        ) : null
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomeScreen;